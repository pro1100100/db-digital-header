#!/usr/bin/env python

counter = 0
with open('data/output/Awesome_Loesung_Teil1.csv', 'r') as f:
    output_file = f.readlines()
validation_file = open('data/expectedResult.csv', 'r')
output_dict = {}
#read all predicted types
for line in output_file:
    (header_id, predicted_type) = line.split(';')
    predicted_type = predicted_type.replace('\n' , '')
    output_dict[header_id]=predicted_type

counter = 0
for line in validation_file:
    line = line.replace('\n','')
    (header_id, expected_type) = line.split(';')
    if(expected_type != output_dict.get(header_id)):
        counter += 1
        print(header_id + '  expected: ' + expected_type + '  predicted: ' + str(output_dict.get(header_id)) + '')


print '\nTOTAL ERRORS:' + str(counter)