#!/usr/bin/python
# -*- encoding: utf-8 -*-
from lib.dataHeader.dataHeader import gettypes
from lib.dataItems.dataItems import getitems
from lib.dataHeader.finereader import update_results, update_results_via_hamming

# first step: get header types (function directly saves types in csv)
#gettypes('documents/c/', 10, outpath='data/output/c/Awesome_Loesung_Teil1.csv', prequel="hc")
#update_results()
#update_results_via_hamming()

# second step: get field data
getitems('documents/c/', 195, output='data/output/c/Awesome_Loesung_Teil2.csv', prequel='hc')
