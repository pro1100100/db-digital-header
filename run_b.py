#!/usr/bin/python
# -*- encoding: utf-8 -*-
from lib.dataHeader.dataHeader import gettypes
from lib.dataItems.dataItems import getitems
import lib.dataHeader.finereader as finereader #update_results, update_results_via_hamming

# first step: get header types (function directly saves types in csv)
gettypes('documents/b/', 700, outpath='data/output/b/Awesome_Loesung_Teil1.csv', prequel='hb')
finereader.update_results()
finereader.update_results_via_hamming()

# second step: get field data
getitems('documents/b/', 300, output='data/output/b/Awesome_Loesung_Teil2.csv', prequel='hb')
