# Document-Parser for DB Digital Header challenge

## 1. Requirements
  * opencv 3.0+ *Make sure python know where to find proper opencv lib*
  * python 2.7+

## 2. Setup
```
./setup.sh
```
## 3. Usage
```
./run.sh
```

###4. Use of traineddata
```
sudo cp traineddata/deu.traineddata /usr/local/share/tessdata/
```

## TODO

1. Haming-Distanz wenn Buchstaben zu mehreren Buchstaben ausgewertet werden.
