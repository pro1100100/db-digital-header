﻿DIS-Beschreibung: BF_GLBd_5740_MPI_01_Ol.dwg          
                                                      
Dokumentation 09/2005 Kohr t DB Telematik GmbH        
                             D.KTR-S-P                
                             Landsberger Straße 314   
                             80687 München            
                                    Datum   Name      
                             Gez.   02/2005 Kaulfuß   
                             Bearb. 06/2005 Zeuner    
                             Gepr.  06/2005 Eberhardt 
V W           Datum   Name |Urspr.                    
Änderung                                              
Maßstab: ohne               Format: A3        
Planzustand: Bestand freigegeben              
• •                                           
Ubersichtsplan Bedienplatz                    
ART-Anbindung                                 
"MPI" (01) Piding                             
Strecke 5740                                  
Freilassing, W208 - Bad Reichenhall           
Dokument-ID: Tkap_2057006700.dwg       Blatt  
                                       01     
                                       01 Bl. 
Ers.f.: BF GLBd 5740 MPI 01 01. dwg           
