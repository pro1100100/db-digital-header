﻿DIS-Beschreibung: BF_GLBd_5741_MHTM_01_01.dwg         
                             Pie    Patan 151         
Dokumentation 09/2005 Kohr t DB Telematik GmbH        
                             D.KTR-S-P                
                             Landsberger Straße 314   
                             80687 München            
                                    Datum   Name      
                             Gez.   02/2005 KaulfuH   
                             Bearb. 02/2005 Zeuner    
                             Gepr.  02/2005 Eberhardt 
V W                                                   
Änderung      Datum   Name Urspr.                     
Maßstab: ohne       Format: A3              
Planzustand: Bestand freigegeben            
• •                                         
Ubersichtsplan Bedienplatz                  
ART-Anbindung                               
"MHTM" (01) Hallthurm                       
Strecke 5741                                
Bad Reichenhall - Berchtesgaden Hbf         
Dokument-ID:                         Blatt  
Tkap_2057006706.dwg                  01     
                                     01 Bl. 
| Ers.f.: BF GLBd 5741 MHTM 01 Ol.dwq