﻿DIS-Beschreibung: BF_GLBd_5520_MKFG_01_01.dwg         
                                                      
Dokumentation 08/2008 Kohr t DB Telematik GmbH        
                             D.KTR-S-P                
                             Landsberger Straße 314   
                             80687 München            
                                    Datum   Name      
                             Gez.   07/2004 E.-Potsch 
                             Bearb. 02/2005 Bauer     
                             Gepr.  02/2005 Eberhardt 
V W           Datum   Name |Urspr.                    
Änderung                                              
Maßstab: ohne              Format: A3   
Planzustand: Bestand feigegeben         
• •                                     
Ubersichtsplan Bedienplatz              
ART-Anbindung                           
"MKFG" (01) Kaufering                   
Strecke 5520                            
München-Pasing, W 330 - Buchloe         
Dokument-ID:                     Blatt  
Tkat2057006773.dwg               01     
                                 01 Bl. 
Ers.f.: BF GLBd 5520 MKFG 01 Ol.dwg     
