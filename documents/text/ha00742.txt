﻿DIS-Beschreibung: BF_BG_5540_MMDN_02_01.dwg
                                Die Bahn                     
Dokumentation 01/2006 K o h r t DB Telematik GmbH            
                                D.KTR-S-P                    
                                Landsberger Straße 314       
                                80687 München                
                                       Datum      Name       
                                Gez.   03/2005    E.-Potsch  
                                Bearb. 03/2005    Zeuner     
                                Gepr.  03/2005    Eberhardt  
9 9                                                          
Änderung      Datum   Name Urspr.                            
Maßs^ab; ohne
Format: A3.2
Planzustand: Bestand freigegeben
Gestellschrank-Belegungsplan
ART-Anbindung
"MMDN" (02) München Donnerbergerbrücke Strecke 5540
München Hbf (Tunnelbf) - Gaufing, W22, S-Bahn
Blatt
01
01
Bl.
Ers.f.: BF
BG
5540
MMDN
02
Ol.dwg