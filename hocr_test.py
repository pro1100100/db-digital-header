#!/usr/bin/env python

import sys
import os.path
from lib._hocr import hocr, parse_hocr

img_file_path = sys.argv[1]
program_path = os.path.dirname(os.path.abspath(__file__))
image_path = os.path.join(program_path, img_file_path)


hocr_text = hocr(image_path)
parsed_hocr_text = parse_hocr(hocr_text)
for element in parsed_hocr_text.iter():
    print("%s - %s" % (element.tag, element.text))
    for name, value in sorted(element.items()):
        print('%s = %r' % (name, value))
    print ''
    print "================================"