#!/usr/bin/python
# -*- encoding: utf-8 -*-
from lib.dataHeader.dataHeader import gettypes
from lib.dataItems.dataItems import getitems
from lib.dataHeader.finereader import update_results, update_results_via_hamming

# first step: get header types (function directly saves types in csv)
gettypes('documents/', 201)
update_results()
update_results_via_hamming()

# second step: get field data
#getitems('documents/', 10 )