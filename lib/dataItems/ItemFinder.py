from lib.share._image_math import *
from lib.share._string_operations import *
from lib.share import knowledge_data
from lib.share._hamming import hamming_dist
from change_item import try_to_find_change_items
import re


class ItemFinder:
    ten_digit_number = re.compile('\d{10}')
    ohne = re.compile('ohne')
    massstab = re.compile('[0-9]+:[0-9]+')
    planzeichen_0 = re.compile('([T|t][K|k][A-Za-z]{2})_')
    planzeichen_1 = re.compile('[T|t][K|k][A-Za-z]{2}')
    # planzeichen_2 = re.compile('^([A-Za-z]{4})_', re.M)
    # planzeichen_3 = re.compile('^[A-Z]{4}$')
    # planzeichen_4 = re.compile('^[A-Z]{1}[a-z]{3}$')
    bl_0 = re.compile('(\d+)\s*Bl.')
    bl_1 = re.compile('Bl.\s*(\d+)')
    km_0 = re.compile('km\s*(\d+,\d+)\s*(-)\s*(km)\s*(\d+,\d+)')
    km_1 = re.compile('(\d+,d+)\s*(-)\s*(\d+,d+)')
    km_2 = re.compile('km\s*(\d+,\d+)')
    blatt_0 = re.compile('Blatt\.(\d*)', re.S)
    blatt_1 = re.compile('Blatt\s*(\d*)')
    format_0 = re.compile('A\d')
    format_1 = re.compile('\w{1,5}x\w{1,5}')
    create_for = re.compile('Ers\.[ |]f\.:')
    dis = re.compile('DIS.*Beschreibung:(.*)')
    doc_id = re.compile('Dokument.*ID:(.*)')
    edit_by = re.compile('Bearbeitet[ |]durch:')
    plan_create = re.compile('Planerstellung:(.*)')
    owner = re.compile('Eigent\\xc3\\xbcmer:(.*)')

    @staticmethod
    def try_to_optimize_line(line_arr):
        if len(line_arr[25]) > 2 and ItemFinder.ohne.findall(line_arr[25].lower()) == []:  # A2, A3 usw.
            line_arr[25] = ItemFinder.replace_letter_to_digit(line_arr[25])  # Format
        if ItemFinder.ohne.findall(line_arr[29].lower()) == []:
            line_arr[29] = ItemFinder.replace_letter_to_digit(line_arr[29])  # Massstab
            line_arr[29] = ItemFinder.replace_par_to_zero(line_arr[29])  # Massstab
            line_arr[29] = line_arr[29].replace(';', ' ')
        line_arr[29] = line_arr[29].split('Format')[0]  # remove wrong 'Format' field
        line_arr[29] = line_arr[29].split('Ur')[0]  # remove wrong field 'Urspr'
        return line_arr


    def find_node_by_value(self, nodeval):
        for image_node in self.image_nodes:
            clean_node_lines = remove_empty_lines(image_node.content.split('\n'))
            for line in clean_node_lines:
                line = remove_umlaute(line)
                position = line.find(nodeval)
                if position > -1:
                    return image_node
                else:
                    hamming_distance, start, end = hamming_dist(nodeval, line)
                    hamming_distance_percentage = hamming_distance / (len(nodeval) + 0.0)
                    if hamming_distance_percentage < 0.14:
                        return image_node

    @staticmethod
    def replace_letter_to_digit(str):
        # print str
        str = str.replace('A', '4')
        str = str.replace('L', '4')
        str = str.replace('h', '4')
        str = str.replace('S', '5')
        str = str.replace('s', '5')
        return str

    @staticmethod
    def replace_par_to_zero(str):
        # print str
        str = str.replace('()', '0')

        return str

    def __init__(self, plan_id, image_path):
        self.plan_id = plan_id
        orig_image = cv2.imread(image_path, 0)
        margin = 8
        image_nodes = []
        self.clean_image_nodes = []
        while len(image_nodes) == 0 and margin < 20:
            try:
                image_nodes = cut_image(orig_image, margin=margin)
            except cv2.error as e:
                if margin < 20:
                    margin += 1
                    print(e)
                    pass

        self.clean_image_nodes = filter((lambda x: (x.content != '' and x.content != ' ')), image_nodes)
        self.image_nodes = image_nodes
        self.result_line = [None] * (len(knowledge_data.item_keys) + 1)

    def to_str(self):
        output = ''
        for img_node in self.image_nodes:
            output += img_node.to_str() + "\n"
        return output

    def find_items(self):
        self.find_primitive()
        self.try_to_find_special_items()
        self.result_line = ItemFinder.try_to_optimize_line(self.result_line)
        self.find_multifeld()
        self.do_some_specific_functions()
        try_to_find_change_items(self)
        return ';'.join(self.result_line)

    def do_some_specific_functions(self):
        # ADD dynamic item-specific functions HERE
        self.update_scale_Item()
        self.find_signed_edit_checked()
        self.find_signed_checked_over()

    def find_signed_checked_over(self):
        if self.result_line[38] != "":  # already found with other method
            return
        check_val = 'gepr'
        over_val = 'ubere'
        check_node = None
        for image_node in self.clean_image_nodes:
            line = remove_umlaute(image_node.content.replace('\n', ' '))
            line = line.lower()
            position = line.find(over_val)
            if position > -1:  # found node with "Ubere."->  check if node over is "Ubere."
                node_about = self.find_neighbor_cell(image_node, 0)
                if node_about is not None:
                    hamming_distance, start, end = hamming_dist(check_val, remove_umlaute(node_about.content.lower()))
                    hamming_distance_percentage = hamming_distance / (len(check_val)+0.0)
                    if hamming_distance_percentage < 0.4:  # short word
                        check_node = node_about
                        break
            else:
                hamming_distance, start, end = hamming_dist(over_val, line)
                hamming_distance_percentage = hamming_distance / (len(over_val)+0.0)
                if hamming_distance_percentage < 0.30:
                    node_over = self.find_neighbor_cell(image_node, 0)
                    if node_over is not None:
                        hamming_distance, start, end = hamming_dist(check_val, remove_umlaute(node_over.content.lower))
                        hamming_distance_percentage = hamming_distance / (len(check_val)+0.0)
                        if hamming_distance_percentage < 0.4:  # short word
                            check_node = node_over
                            break

        if check_node is None:
            return

        signed_node = self.find_neighbor_cell(check_node, 0)
        about_node = self.find_neighbor_cell(check_node, 2)

        if signed_node is not None:
            signed_date = self.find_neighbor_cell(signed_node, 1)
            if signed_date is not None:
                self.result_line[39] = self.check_for_empty_field(signed_date.content)
                signed_name = self.find_neighbor_cell(signed_date, 1)
                if signed_name is not None:
                    self.result_line[40] = self.check_for_empty_field(signed_name.content)

        about_date = self.find_neighbor_cell(about_node, 1)
        if about_date is not None:
            self.result_line[43] = self.check_for_empty_field(about_date.content)
            about_name = self.find_neighbor_cell(about_date, 1)
            if about_name is not None:
                self.result_line[44] = self.check_for_empty_field(about_name.content)

        if check_node is not None:
            checked_date = self.find_neighbor_cell(check_node, 1)
            if checked_date is not None:
                self.result_line[38] = self.check_for_empty_field(checked_date.content)
                checked_name = self.find_neighbor_cell(checked_date, 1)
                if checked_name is not None:
                    self.result_line[42] = self.check_for_empty_field(checked_name.content)

    def find_signed_edit_checked(self):
        edit_val = 'bearb'
        check_val = 'gepr'
        edit_node = None
        for image_node in self.clean_image_nodes:
            line = remove_umlaute(image_node.content.replace('\n', ' '))
            line = line.lower()
            position = line.find(edit_val)
            if position > -1:  # found node with "Bearb."->  check if node below is "Gepr."
                node_below = self.find_neighbor_cell(image_node, 2)
                if node_below is not None:
                    hamming_distance, start, end = hamming_dist(check_val, remove_umlaute(node_below.content.lower()))
                    hamming_distance_percentage = hamming_distance / (len(check_val)+0.0)
                    if hamming_distance_percentage < 0.4:  # short word
                        edit_node = image_node
                        break
            else:
                hamming_distance, start, end = hamming_dist(edit_val, line)
                hamming_distance_percentage = hamming_distance / (len(edit_val)+0.0)
                if hamming_distance_percentage < 0.30:
                    node_below = self.find_neighbor_cell(image_node, 2)
                    if node_below is not None:
                        hamming_distance, start, end = hamming_dist(check_val, remove_umlaute(node_below.content.lower()))
                        hamming_distance_percentage = hamming_distance / (len(check_val)+0.0)
                        if hamming_distance_percentage < 0.4:  # short word
                            edit_node = image_node
                            break

        if edit_node is None:
            return

        signed_node = self.find_neighbor_cell(edit_node, 0)
        checked_node = self.find_neighbor_cell(edit_node, 2)

        if signed_node is not None:
            signed_date = self.find_neighbor_cell(signed_node, 1)
            if signed_date is not None:
                self.result_line[39] = self.check_for_empty_field(signed_date.content)
                signed_name = self.find_neighbor_cell(signed_date, 1)
                if signed_name is not None:
                    self.result_line[40] = self.check_for_empty_field(signed_name.content)

        edit_date = self.find_neighbor_cell(edit_node, 1)
        if edit_date is not None:
            self.result_line[37] = self.check_for_empty_field(edit_date.content)
            edit_name = self.find_neighbor_cell(edit_date, 1)
            if edit_name is not None:
                self.result_line[41] = self.check_for_empty_field(edit_name.content)

        if checked_node is not None:
            checked_date = self.find_neighbor_cell(checked_node, 1)
            if checked_date is not None:
                self.result_line[38] = self.check_for_empty_field(checked_date.content)
                checked_name = self.find_neighbor_cell(checked_date, 1)
                if checked_name is not None:
                    self.result_line[42] = self.check_for_empty_field(checked_name.content)

    def check_for_empty_field(self, value):
        if value is None or value == '':
            return "empty Field"
        else:
            return value

    def update_scale_Item(self):
        # found 'Massstab' but no value
        if self.result_line[29] == "empty Field":  # look in cell to the left for specific value
            scale_item = self.find_node_by_value("Massstab")
            if scale_item is None:
                return
            right_node = self.find_neighbor_cell(scale_item, 1)
            if right_node is None:
                return
            temp = right_node.content
            if ItemFinder.ohne.findall(temp.lower()) == []:
                temp = ItemFinder.replace_letter_to_digit(temp)  # Massstab
                temp = ItemFinder.replace_par_to_zero(temp)  # Massstab
                temp = temp.replace(';', ' ')
                if ItemFinder.massstab.findall(temp)!= []:
                    self.result_line[29] = temp
            else:
                # found key-word "ohne"
                self.result_line[29] = temp

    def find_largest_node(self):
        largest_node = ''
        for image_node in self.clean_image_nodes:
            if len(image_node.content) > len(largest_node):
                largest_node = image_node.content
        return largest_node

    def find_multifeld(self):
        self.result_line[53] = self.find_largest_node().replace("\n", " ").replace(";", " ")

    def find_neighbor_cell(self, cur_cell, direction=0 , width_div = 1, height_div = 1):
        cur_x1 = cur_cell.x1
        cur_x2 = cur_cell.x2
        cur_y1 = cur_cell.y1
        cur_y2 = cur_cell.y2
        min_x1 = cur_x1 - 50
        max_x1 = cur_x1 + 50
        min_x2 = cur_x2 - 50
        max_x2 = cur_x2 + 50
        height = (cur_cell.y2 - cur_y1)/ height_div
        min_y1 = cur_y1 - max(height / 3, 25)
        max_y1 = cur_y1 + max(height / 3, 25)
        min_y2 = cur_y2 - max(height / 3, 25)
        max_y2 = cur_y2 + max(height / 3, 25)
        width = (cur_x2 - cur_x1)/ width_div
        for node in self.image_nodes:
            if node == cur_cell or (node.x2 - node.x1) < 4 or (node.y2 - node.y1) < 4:  #ignore same cell or too small cells
                continue
            if direction == 0:  # find cell above
                if not (width - 30 < node.x2 - node.x1 < width + 30):  # cell  is too small or too big
                    continue

                if min_y1 < node.y2 < max_y1 and min_x1 < node.x1 < max_x1:
                    return node
            elif direction == 1:  # find cell to the right
                if min_y1 < node.y1 < max_y1 and min_x2 < node.x1 < max_x2:
                    return node
            elif direction == 2:  # find cell below
                if not (width - 30 < node.x2 - node.x1 < width + 30):  # cell  is too small or too big
                    continue
                if min_y2 < node.y1 < max_y2 and min_x1 < node.x1 < max_x1:
                    return node
            else:  # find cell to the left
                if min_y1 < node.y1 < max_y1 and min_x1 < node.x2 < max_x1:
                    return node
        return None

    def find_primitive(self):
        self.result_line[0] = self.plan_id
        for idx, item in enumerate(knowledge_data.item_keys):
            temp_item_value = ''
            temp_item_len = 0
            clean_item = item.encode('utf-8')
            found_item = False
            item_len = len(clean_item) + 0.0
            max_allowed_hamming_distance = 0.12
            while temp_item_value == '' and max_allowed_hamming_distance <= 0.2:
                # print 'Max allowed dist: ', max_allowed_hamming_distance
                for image_node in self.clean_image_nodes:
                    clean_node_lines = remove_empty_lines(image_node.content.split('\n'))
                    for line in clean_node_lines:
                        position = line.find(clean_item)
                        if position > -1:
                            # found header in current line
                            if item_len > temp_item_len:
                                temp_item_len = item_len
                                temp_item_value = self.get_item_value(line, clean_node_lines,
                                                                      int(position + temp_item_len), item)
                                found_item = True
                                break  # quit loop for line...
                        else:
                            hamming_distance, start, end = hamming_dist(clean_item, line)
                            hamming_distance_percentage = hamming_distance / item_len
                            if hamming_distance_percentage < max_allowed_hamming_distance:
                                # found header in current line
                                # print 'Hit'
                                if item_len > temp_item_len:
                                    temp_item_len = item_len
                                    temp_item_value = self.get_item_value(line, clean_node_lines, end, item)
                                    found_item = True
                                    break  # quit loop for line...
                    if found_item:
                        break  # quit loop for image_node ...
                # found no matching type -> increase distance and do again
                max_allowed_hamming_distance += 0.02
            self.result_line[idx + 1] = temp_item_value

    def get_item_value(self, line, all_lines, position, item):
        # print all_lines
        value = line[position:]
        if value.strip():
            return value.lstrip()
        else:
            line_index = all_lines.index(line)
            if line_index + 1 < len(all_lines):
                return all_lines[line_index + 1]
        return 'empty Field'

    def try_to_find_special_items(self):
        for node in self.clean_image_nodes:
            try:
                ten_digit = ItemFinder.ten_digit_number.search(node.content).group(0)
                self.result_line[49] = ten_digit  # 10 stellige nummer
            except AttributeError:
                pass

            if self.result_line[21] == ' ' or self.result_line[21] == '': # only check if new entry was already found
                try:
                    created_for = ItemFinder.create_for.search(node.content).group(1)
                    self.result_line[21] = created_for.strip()
                    if self.result_line[21] == "":
                        self.result_line[21] = 'empty Field'
                except IndexError:  # group 1 don't exist -> empty Field
                    self.result_line[21] = 'empty Field'
                except AttributeError:
                    pass

            try:
                document_id = ItemFinder.doc_id.search(node.content.replace('\n',' ')).group(1)
                self.result_line[18] = document_id.strip()
                if self.result_line[18] == "":
                    self.result_line[18] = 'empty Field'
            except IndexError:  # group 1 don't exist -> empty Field
                self.result_line[18] = 'empty Field'
            except AttributeError:
                pass

            try:
                dis_beschreibung = ItemFinder.dis.search(node.content).group(1)
                self.result_line[17] = dis_beschreibung.strip()
                if self.result_line[17] == "":
                    self.result_line[17] = 'empty Field'
            except IndexError:  # group 1 don't exist -> empty Field
                self.result_line[17] = 'empty Field'
            except AttributeError:
                pass

            try:
                eigentuemer = ItemFinder.owner.search(node.content.replace('\n', ' ')).group(1)
                self.result_line[19] = eigentuemer.strip()
                if self.result_line[19] == "":
                    self.result_line[19] = 'empty Field'
            except IndexError:  # group 1 don't exist -> empty Field
                self.result_line[19] = 'empty Field'
            except AttributeError:
                pass

            try:
                plan_erstellung = ItemFinder.plan_create.search(node.content.replace('\n', ' ')).group(1)
                self.result_line[31] = plan_erstellung.strip()
                if self.result_line[31] == "":
                    self.result_line[31] = 'empty Field'
            except IndexError:  # group 1 don't exist -> empty Field
                self.result_line[31] = 'empty Field'
            except AttributeError:
                pass

            try:
                planzeichen = ItemFinder.planzeichen_0.search(node.content).group(1)
                self.result_line[54] = planzeichen  # planzeichen
            except AttributeError as e:
                try:
                    planzeichen = ItemFinder.planzeichen_1.search(node.content).group(0)
                    self.result_line[54] = planzeichen  # planzeichen
                except AttributeError as e:
                    try:
                        planzeichen = ItemFinder.planzeichen_2.search(node.content).group(1)
                        self.result_line[54] = planzeichen  # planzeichen
                    except AttributeError:
                        try:
                            planzeichen = ItemFinder.planzeichen_3.search(node.content).group(0)
                            self.result_line[54] = planzeichen  # planzeichen
                        except AttributeError:
                            try:
                                planzeichen = ItemFinder.planzeichen_4.search(node.content).group(0)
                                self.result_line[54] = planzeichen  # planzeichen
                            except AttributeError:
                                pass
            try:
                bl = ItemFinder.bl_0.search(node.content).group(1)
                self.result_line[14] = bl  # Bl.
            except AttributeError:
                try:
                    bl = ItemFinder.bl_1.search(node.content).group(1)
                    self.result_line[14] = bl  # Bl.
                except AttributeError:
                    if not self.result_line[14].strip():
                        self.result_line[14] = 'empty Field'
                    pass

            try:
                km_groups = ItemFinder.km_0.search(node.content)
                # print km_groups.groups()
                km = " ".join(km_groups.group(1), km_groups.group(2), km_groups.group(3))
                self.result_line[47] = km  # km.
            except (AttributeError, TypeError) as e:
                try:
                    km_groups = ItemFinder.km_1.search(node.content)
                    km = " ".join(km_groups.groups())
                    self.result_line[47] = km  # km.
                except AttributeError:
                    try:
                        km = ItemFinder.km_2.search(node.content).group(1)
                        self.result_line[47] = km  # km.
                    except AttributeError:
                        if not self.result_line[47].strip():
                            self.result_line[47] = 'empty Field'
                        pass

            try:
                blatt = ItemFinder.blatt_0.search(node.content).group(1)
                self.result_line[15] = blatt  # Blatt
            except AttributeError:
                try:
                    blatt = ItemFinder.blatt_1.search(node.content).group(1)
                    self.result_line[15] = blatt  # Blatt
                except AttributeError:
                    if not self.result_line[15].strip():
                        self.result_line[15] = 'empty Field'
                    pass


            try:
                formatt = ItemFinder.format_0.search(node.content).group()
                self.result_line[25] = formatt  # Format
            except AttributeError:
                try:
                    formatt = ItemFinder.format_1.search(node.content).group()
                    self.result_line[25] = formatt  # Format
                except AttributeError:
                    if not self.result_line[25].strip():
                        self.result_line[25] = 'empty Field'
                    pass
