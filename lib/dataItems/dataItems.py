import sys
import os.path
import time
from lib.dataItems.ItemFinder import ItemFinder
from PIL import Image
from multiprocessing import Pool, Manager, cpu_count

PROGRAM_PATH = os.path.dirname(os.path.abspath(__file__))
OUTFILE = ''
OUTPATH = 'data/output/Awesome_Loesung_Teil2.csv'
DATAFOLDER = ''
PREQUEL = 'ha'
__MANAGER = Manager()
ITEMS_DICT = __MANAGER.dict()


def getitems(datafolder, max_range= 10, pools=0 , output ='', prequel='ha'):
    global OUTPATH, PREQUEL
    PREQUEL = prequel
    if output != '':
        OUTPATH = output
    if pools == 0:
        pools = cpu_count()
    print('Starting item extraction using ' + str(pools) + ' workers...')

    global DATAFOLDER
    DATAFOLDER = datafolder
    p = Pool(pools)
    res = p.map_async(_extract_items, range(1, max_range))
    p.close()
    while res._number_left > 0:
        sys.stdout.write("Waiting for " + str(max_range - 1 - len(ITEMS_DICT)) + " files to finish...\r")
        sys.stdout.flush()
        time.sleep(5)

    write_items_to_file()


def _extract_items(plan_number):
    global PREQUEL
    try:
        plan_id = PREQUEL + str(plan_number).zfill(5)
        #print('Starting extraction of: ' + plan_id)
        if plan_number%30 == 0:
            print plan_number
        img_file_path = '../../'+ DATAFOLDER + plan_id + ".png"
        img_path = os.path.join(PROGRAM_PATH, img_file_path)
        image = Image.open(img_path)
        item_finder = ItemFinder(plan_id, img_path)
        line = item_finder.find_items()
        line = line.replace('"', ' ')
        ITEMS_DICT[plan_id] = line.replace("\n", " ")
        #print('Finished extraction of: ' + plan_id)

    except (OSError, IOError) as e:
        print(str(e) + ', continue...')
    except Exception as e:
        print('Unhandled exception in '+plan_id +' : ' + str(e) + ', continue...')


def write_items_to_file():
    global ITEMS_DICT, OUTPATH, OUTFILE
    output = ''
    dictionary = {}
    for item_key in iter(ITEMS_DICT.keys()):
        dictionary[item_key] = ITEMS_DICT[item_key]

    for item_key in sorted(dictionary):
        output += ITEMS_DICT[item_key] + '\n'

    OUTFILE = open(OUTPATH,'w')
    OUTFILE.write(output)
