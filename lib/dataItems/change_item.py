from lib.share._hamming import hamming_dist
from lib.share._string_operations import *


def try_to_find_change_items(item_finder):
     map(lambda x: __check_for_change_and_fill_up(item_finder, x), item_finder.clean_image_nodes)


def __check_for_change_and_fill_up(item_finder,node):
    change_string = 'Anderung'
    len_change_string = (len(change_string) + 0.0)
    line = remove_umlaute(node.content)
    position = line.find(change_string)
    if -1 < position:  # don't find lines with other changes
        fill_up_change_item(item_finder, node)
    else:
        hamming_distance, start, end = hamming_dist(change_string, line)
        hamming_distance_percentage = hamming_distance / len_change_string
        if hamming_distance_percentage < 0.1:
            fill_up_change_item(item_finder, node)

def fill_up_change_item(item_finder, cur_node):
    change_type = -1  # differ in different change combination
    # find next cell to the right
    right_node = item_finder.find_neighbor_cell(cur_node, 1)
    if right_node is not None and right_node.content == "": #empty cell-> take next one, sometimes wrong cutting
        right_node = item_finder.find_neighbor_cell(right_node, 1)

    if right_node is not None:
        start = check_for_param_and_cut(remove_umlaute(right_node.content), 'Gezeichnet', 'Gez.')
        if start > -1:  # found parameter
            change_type = 0  # parameter only exist in first type of changes
            fill_up_change_type_0(item_finder,cur_node, 2)  # fill up item with label 'Anderung'
            fill_up_change_type_0(item_finder, right_node, 3)
            right_node = item_finder.find_neighbor_cell(right_node, 1)  # get next cell
        else:
            change_type = 1  # different change type
    else:
        return

    if right_node is not None and right_node.content == "": #empty cell-> take next one, sometimes wrong cutting
        right_node = item_finder.find_neighbor_cell(right_node, 1)
    if right_node is not None:
        start = check_for_param_and_cut(remove_umlaute(right_node.content), 'Datum')
        if start > -1 and change_type == 0:  # found parameter
            fill_up_change_type_0(item_finder, right_node, 4)
            right_node = item_finder.find_neighbor_cell(right_node, 1)
        elif start > -1 and change_type == 1:
            fill_up_change_type_1(item_finder,cur_node, 9)  # fill up item with label 'Anderung'
            fill_up_change_type_1(item_finder, right_node, 10)
            right_node = item_finder.find_neighbor_cell(right_node, 1)
    else:
        # found no match
        return

    if change_type == 1: # next item is "Name"
        if right_node is not None and right_node.content == "":  # empty cell-> take next one, sometimes wrong cutting
            right_node = item_finder.find_neighbor_cell(right_node, 1)
        if right_node is not None:
            start = check_for_param_and_cut(remove_umlaute(right_node.content), 'Name')
            if start > -1:  # found parameter
                fill_up_change_type_1(item_finder, right_node, 11)
                right_node = item_finder.find_neighbor_cell(right_node, 1)

        return  # change type 1 terminates here

    #  only change type 0 reach here
    #  next item is "Gepruft"
    if right_node is not None and right_node.content == "": #empty cell-> take next one, sometimes wrong cutting
        right_node = item_finder.find_neighbor_cell(right_node, 1)
    if right_node is not None:
        start = check_for_param_and_cut(remove_umlaute(right_node.content), 'Gepruft', 'Gepr.')
        if start > -1:  # found parameter
            fill_up_change_type_0(item_finder, right_node, 5)
            right_node = item_finder.find_neighbor_cell(right_node, 1)
    else:
        return

    if right_node is not None and right_node.content == "": #empty cell-> take next one, sometimes wrong cutting
        right_node = item_finder.find_neighbor_cell(right_node, 1)
    if right_node is not None:
        start = check_for_param_and_cut(remove_umlaute(right_node.content), 'Datum')
        if start > -1 and change_type == 0:  # found parameter
            fill_up_change_type_0(item_finder, right_node, 6)
    else:
        return

    right_node = item_finder.find_neighbor_cell(right_node, 1)
    if right_node is not None and right_node.content == "":  # empty cell-> take next one, sometimes wrong cutting
        right_node = item_finder.find_neighbor_cell(right_node, 1)
    if right_node is not None:
        start = check_for_param_and_cut(remove_umlaute(right_node.content), 'Ubere v. Ort', 'Ubere.')
        if start > -1:  # found parameter
            fill_up_change_type_0(item_finder, right_node, 7)
    else:
        return

    right_node = item_finder.find_neighbor_cell(right_node, 1)
    if right_node is not None and right_node.content == "": #empty cell-> take next one, sometimes wrong cutting
        right_node = item_finder.find_neighbor_cell(right_node, 1)
    if right_node is not None:
        start = check_for_param_and_cut(remove_umlaute(right_node.content), 'Datum')
        if start > -1 and change_type == 0:  # found parameter
            fill_up_change_type_0(item_finder, right_node, 8)
    else:
        return


def fill_up_change_type_0(item_finder, cur_node, item_index):
    node_above = item_finder.find_neighbor_cell(cur_node, 0)

    if node_above is not None and node_above.content != '':
        result_str = node_above.content
        if item_index == 4 or item_index ==  6 or item_index == 8:
            result_str = item_finder.replace_letter_to_digit(result_str)
        item_finder.result_line[item_index] = result_str
    else:
        item_finder.result_line[item_index] = 'empty Field'


def fill_up_change_type_1(item_finder, cur_node, item_index):
    count = 0
    node_above = item_finder.find_neighbor_cell(cur_node, 0)
    while count < 10:
        if node_above is not None and node_above.content != '':
            item_finder.result_line[item_index] = node_above.content
            return
        elif node_above is not None:
            node_above = item_finder.find_neighbor_cell(node_above, 0)
            count += 1
        else:
            break
    item_finder.result_line[item_index] = 'empty Field'




def check_for_param_and_cut(line, default_parameter, fallback_parameter=None):
    line, start, end = check_for_change_parameter(line, default_parameter)
    if start == -1 and fallback_parameter is not None:  # no match, but falback defined
        line, start, end = check_for_change_parameter(line, fallback_parameter)
    return start

def check_for_change_parameter(line, change_parameter):
    position = line.find(change_parameter)
    parameter_len = len(change_parameter)
    if position > -1:
        line = line[position + parameter_len:]  # cut off start
        return line, position, position + parameter_len
    else:
        hamming_distance, start, end = hamming_dist(change_parameter, line)
        if hamming_distance / (parameter_len + 0.0) < 0.18:
            line = line[end + 1:]  # cut off start
            return line, start, end
    # fallback
    return line, -1, -1
