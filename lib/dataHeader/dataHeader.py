import sys
from lib.share import knowledge_data
from lib.share._hamming import try_hamming_distance
import os.path
from PIL import Image
import pytesseract
import time
from multiprocessing import Pool, Manager, cpu_count
from lib.share._string_operations import *

PROGRAM_PATH = os.path.dirname(os.path.abspath(__file__))
DATAFOLDER = ''
OUT_PATH = 'data/output/Awesome_Loesung_Teil1.csv'
__MANAGER = Manager()
HEADERTYPES_DICT = __MANAGER.dict()
PREQUEL = 'ha'


def gettypes(datafolder, max_range=915, pools=0, outpath=" ", prequel='ha'):
    global OUT_PATH, PREQUEL
    PREQUEL = prequel
    if outpath != " ":
        OUT_PATH = outpath
    if pools == 0:
        pools = cpu_count()
    print 'Starting type classification using ' + str(pools) + ' workers...'

    global DATAFOLDER
    DATAFOLDER = datafolder
    p = Pool(pools)
    res = p.map_async(_classify_file, range(1, max_range))
    #res.get()
    p.close()
    while res._number_left > 0:
        sys.stdout.write("Waiting for " + str(max_range - 1 - len(HEADERTYPES_DICT)) + " files to finish...\r")
        sys.stdout.flush()
        time.sleep(5)

    write_classified_types_to_file()


def write_classified_types_to_file():
    global HEADERTYPES_DICT, OUT_PATH
    output = ''
    dictionary = {}
    for typ_key in iter(HEADERTYPES_DICT.keys()):
        dictionary[typ_key] = HEADERTYPES_DICT[typ_key]
    for typ_key in sorted(dictionary):
        output += typ_key + ';' + HEADERTYPES_DICT[typ_key] + '\n'
    OUTFILE = open(OUT_PATH, 'w')
    OUTFILE.write(output)


# internal subfunction
def _classify_file(plan_number):
    global  PREQUEL
    try:  # missing fileIDs
        plan_id = PREQUEL + str(plan_number).zfill(5)
        img_file_path = '../../' + DATAFOLDER + plan_id + ".png"

        image_path = os.path.join(PROGRAM_PATH, img_file_path)

        image = Image.open(image_path)
        inputfile = pytesseract.image_to_string(image, lang='deu', config='-psm 4')

        res = _getheadertype(inputfile)
        global HEADERTYPES_DICT
        HEADERTYPES_DICT[plan_id] = res

    except (OSError, IOError) as e:
        print e, ', continue...'


def _getheadertype(inputstring):
    # first step: remove empty lines)
    lines = inputstring.split("\n")
    clean_lines = remove_empty_lines(lines)
    # second step: import header type and compare
    headertypes = knowledge_data.types

    temp_header = ' '  # use of temp_header to find better results
    temp_header_points = 0.0
    cur_hamming_distance = 0.12
    while temp_header == ' ' and cur_hamming_distance <= 0.25:
        # loop over clean lines
        for j in range(0, len(clean_lines)):
            cur_clean_line = remove_umlaute(clean_lines[j]).strip()
            # loop over defined header types
            for i in range(0, len(headertypes)):
                cur_header = headertypes[i].encode('utf-8')
                cur_clean_header = remove_umlaute(cur_header)
                header_len = len(cur_clean_header) + 0.0  # mark as float
                if cur_clean_header in cur_clean_line:

                    # found header in current line
                    if header_len > temp_header_points:
                        temp_header = cur_header  # found better header
                        temp_header_points = header_len
                else:
                    distance = try_hamming_distance(cur_clean_header, cur_clean_line)/header_len
                    if distance< cur_hamming_distance and header_len*(1-(distance/1)) > temp_header_points:
                        # found header in current line
                        temp_header = cur_header  # found better header
                        temp_header_points = header_len*(1-(distance/1))

                    # idea: get length of cur_header as x
                    # take first x chars and compare
                    # try hamming distance
                    # after that move on (one char)
                    # take solution if distance < 10% (len divided by errors)
                    # compare with cur solution -> take longer solution

        # found no matching type -> increase distance and do again
        cur_hamming_distance += 0.02

    # third step: return match
    if temp_header == ' ':
        temp_header = 'unknown'
    return temp_header
