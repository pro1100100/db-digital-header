import os.path
from lib.share import knowledge_data
from lib.share._hamming import *

solution_file_path = 'data/output/Awesome_Loesung_Teil1.csv'
finereader_docs_path = 'documents/text/'
finereader_file_extension = '.txt'
solution_file = open(solution_file_path, 'r')

def update_results():
	solution_file = open(solution_file_path, 'r')
	sorted_types = sorted(knowledge_data.types, key=len, reverse=True)
	new_results = []
	for line in solution_file:
		plan_id, temp_type = line.split(';')
		file_path = finereader_docs_path + plan_id + finereader_file_extension
		new_temp_result = ''
		if os.path.isfile(file_path) and temp_type == "unknown\n":
			temp_file_str = open(file_path, 'r').read()
			for typ in sorted_types:
				typ_encoded = typ.encode('utf-8')
				typ_position = temp_file_str.find(typ_encoded)
				if typ_position > -1:
					new_temp_result = plan_id+';'+typ_encoded+"\n"
					new_results.append(new_temp_result)
					break
		if new_temp_result == '':
			new_results.append(plan_id + ';' + temp_type)
	output_file = open(solution_file_path, 'w')
	for entry in new_results:
	  output_file.write("%s" % entry)

def update_results_via_hamming():
	sorted_types = sorted(knowledge_data.types, key=len, reverse=True)
	new_results = []
	for line in solution_file:
		plan_id, temp_type = line.split(';')
		file_path = finereader_docs_path + plan_id + finereader_file_extension
		new_temp_result = ''
		if os.path.isfile(file_path) and temp_type == "unknown\n":
			temp_file_str = open(file_path, 'r').read()
			for typ in sorted_types:
				typ_encoded = typ.encode('utf-8')
				hd, start, end = hamming_dist(typ_encoded, temp_file_str)
				hd = hd * 1.0 / len(typ_encoded)
				if hd < 0.2:
					new_temp_result = plan_id+';'+typ_encoded+"\n"
					new_results.append(new_temp_result)
					break
		if new_temp_result == '':
			new_results.append(plan_id + ';' + temp_type)
	output_file = open(solution_file_path, 'w')
	for entry in new_results:
	  output_file.write("%s" % entry)






