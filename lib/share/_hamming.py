#!/usr/bin/python
# -*- encoding: utf-8 -*-
import operator
from itertools import imap


# find smallest hamming distance with current header
def try_hamming_distance(cur_header_type, cur_clean_line):
    min_distance = 1000
    for i in range(0, 2):  # try with missing ending of headertype
        header_len = len(cur_header_type) - i
        temp_header_type = cur_header_type[0:header_len]
        max_range = len(cur_clean_line) - header_len + 1
        min_values = map((lambda x: __get_min_value(cur_clean_line[x:(x + header_len)], temp_header_type)), range(0, max_range))
        if len(min_values) > 0 and min(min_values) < min_distance:
            min_distance = min(min_values)
    return min_distance


# find smallest hamming distance with current header
def hamming_dist(target_string, text_line):
    min_distance = 1000
    start_pos = -1
    end_pos = -1
    target_string_len = len(target_string)
    max_range = len(text_line) - target_string_len + 1
    min_values = map((lambda x: __get_min_value(text_line[x:(x+target_string_len)], target_string)), range(0, max_range))
    if len(min_values) > 0:
        min_distance = min(min_values)
        start_pos = min_values.index(min_distance)
        end_pos = start_pos + target_string_len
    return min_distance, start_pos, end_pos


def __get_min_value(line_sub_string, local_target_string):
    hs = _hamming2(local_target_string, line_sub_string)
    min_distance = hs
    return min_distance


def _hamming2(str1, str2):
    assert len(str1) == len(str2)
    ne = operator.ne
    return sum(imap(ne, str1, str2))
