import yaml
import yamlordereddictloader

#load known document types and items
stram = open("data/doc_types.yml", "r")
types = yaml.load(stram)
stram = open("data/doc_items.yml", "r")
items = yaml.load(stram, Loader=yamlordereddictloader.Loader)
item_keys = items.keys()
