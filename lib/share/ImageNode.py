import pytesseract
from PIL import Image

class ImageNode(object):

    def __init__(self, image, x1, y1, x2, y2):
        self.image = image
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        if x2 - x1 < 4 or y2 - y1 < 4:
            self.content = ""
        else:
            self.content = pytesseract.image_to_string(Image.fromarray(image), lang='deu')
            self.content = self.content.replace('\xe2\x80\x99r', 't')



    def to_str(self):
		return self.content + ' | ' + ', '.join( self.x1, self.y1, self.x2, self.y2 )
