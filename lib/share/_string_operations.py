def remove_umlaute(x):
    """

    :rtype: str
    """
    try:
        result = x.replace('\xc3\x84', 'A')
        result = result.replace('\xc3\x96', 'O')
        result = result.replace('\xc3\x9c', 'U')
        result = result.replace('\xc3\xa4', 'a')
        result = result.replace('\xc3\xb6', 'o')
        result = result.replace('\xc3\xbc', 'u')
        result = result.replace('\xc3\x9f', 'ss')
        result = result.replace('\xe2\x80\x94', '')
        result = result.replace(' ', '')
        result = result.replace('-', '')
        result = result.replace('/', '')
        result = result.replace(',', '')
    except AttributeError :
        return " "

    return result

def remove_empty_lines(lines):
    non_empty_lines = []
    for i in range(0, len(lines)):
        if len(lines[i]) > 0:
            non_empty_lines.append(lines[i])
    return non_empty_lines
