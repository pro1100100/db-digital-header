
import subprocess
from lxml import etree


def hocr(img_file_path, lang='deu'):
	cmd = 'tesseract ' + img_file_path + ' stdout -l '+ lang + ' hocr'
	output = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).communicate()[0]
	return output

def parse_hocr(hocr_text):
	return etree.fromstring(hocr_text)