from __future__ import division
import cv2
import numpy as np
from matplotlib import pyplot as plt
import sys
from ImageNode import ImageNode as IN

sys.setrecursionlimit(100000)


def slope(x1, y1, x2, y2):
    if x1 == x2:
        slope = float('Inf')
    else:
        slope = (y2-y1)/(x2-x1)
    return slope


def is_vertical(x1, y1, x2, y2):
    if slope(x1, y1, x2, y2) > 1:  # assume perfect lines
        return True
    else:
        return False


def is_horizontal(x1, y1, x2, y2):
    return not(is_vertical(x1, y1, x2, y2))


def is_solid_line(x1, y1, x2, y2, h, w):
    line_h = y2-y1
    line_w = x2-x1
    if is_vertical(x1, y1, x2, y2) and line_h >= h*0.85:
        return True
    if is_horizontal(x1, y1, x2, y2) and line_w >= w*0.85:
        return True
    return False


def get_image_lines(image, minLineLength=500, maxLineGap=20):  # minLineLength should be dynamic somehow
    try:
        maxLineGap = max(maxLineGap, 10)
        minLineLength = max(minLineLength, 8)
        edges = cv2.Canny(image, 50, 200, 3)
        lines = cv2.HoughLinesP(edges, 1, np.pi/2, 2, minLineLength=minLineLength, maxLineGap=maxLineGap)
        return lines
    except cv2.error as e:
        print(e)
        return None


# cut image by founded lines
def cut_image(image, x_s=0, y_s=0, lines=[], margin=8):
    height, width = image.shape
    x_e, y_e = x_s + width, y_s + height
    # print width
    lines = get_image_lines(image, width/64, width/400)
    #visualise_lines(image, lines) #debug
    if lines is None:
        try:
            if width < 4 or height < 4:
                return []
            else:
                return [IN(image, x_s, y_s, x_e, y_e)]
        except SystemError as e:
            print('can not create ImageNode...' + e.message)
            return []
    try:
        for idx, line in enumerate(lines):
            x1, y1, x2, y2 = line[0][0], line[0][3], line[0][2], line[0][1]
            solid = is_solid_line(x1, y1, x2, y2, height, width)
            if solid:
                if is_vertical(x1, y1, x2, y2):
                    image1 = image[0: height, 0: max(x1-margin, 1)]
                    image2 = image[0: height, min(x1+margin, width-1): width]

                    x1_start_i1, y1_start_i1 = x_s, y_s  # x_s + x2-margin, y_s + y2 # x1 = x2
                    x1_start_i2, y1_start_i2 = x_s + x1+margin, y_s  # x_e, y_e

                if is_horizontal(x1, y1, x2, y2):
                    image1 = image[0:max(y1-margin, 1), 0:width]
                    image2 = image[min(y1+margin, height-1):height, 0:width]

                    x1_start_i1, y1_start_i1 = x_s, y_s  # x_e, y_s + y2 - margin # y1 = y2
                    x1_start_i2, y1_start_i2 = x_s, y_s + y2 + margin   # x_e, y_e

                if image.shape == image1.shape and image.shape == image2.shape:
                    return [IN(image, x_s, y_s, x_e, y_e)]
                if image.shape == image1.shape: # to avoid endless loop (i.e. ha00836)
                    return [] + cut_image(image2, x1_start_i2, y1_start_i2, margin=margin)
                elif image.shape == image2.shape:
                    return [] + cut_image(image1, x1_start_i1, y1_start_i1, margin=margin)
                else:
                    return [] + cut_image(image1, x1_start_i1, y1_start_i1, margin=margin) + cut_image(image2, x1_start_i2, y1_start_i2, margin=margin)
    except cv2.error as e:
        print(e.message)
        raise cv2.error('Probably to low margin...', margin)
    return [IN(image, x_s, y_s, x_e, y_e)]


# cv2.line(img,(line[0][0],line[0][1]),(line[0][2],line[0][3]),(255,255,255),10)
# print line[0][1]-line[0][3], line[0][2]-line[0][0], is_vertical(line[0][0], line[0][1], line[0][2],line[0][3])

def visualise_lines(gray_image, lines=[], color=(0, 255, 0)):
    height, width = gray_image.shape
    try:
        if lines == []:
            lines = get_image_lines(gray_image, width / 64, width / 400)
        image = cv2.cvtColor(gray_image, cv2.COLOR_GRAY2RGB)
        for line in lines:
            x1, y1, x2, y2 = line[0][0], line[0][1], line[0][2], line[0][3]
            cv2.line(image, (x1, y1), (x2, y2), color, 10)
        show_img(image)
    except TypeError:
        print('cannot vizualize')


def show_img(img):
    plt.subplot(111), plt.imshow(img, cmap='gray')
    plt.title(''), plt.xticks([]), plt.yticks([])
    plt.show()
