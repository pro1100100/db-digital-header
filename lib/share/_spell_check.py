import sys
reload(sys)
sys.setdefaultencoding('utf8')
import enchant

d = enchant.DictWithPWL("de_DE","data/pyenchant_dict.txt")

def contain_typ(target_string, text_line):
    target_string_len = len(target_string)
    for k in range(0, len(text_line) - target_string_len + 1):
        end = k + target_string_len
        line_sub_string = text_line[k:end]
        suggestions = d.suggest(line_sub_string)
        #print target_string, line_sub_string, suggestions
        if suggestions != []:
			suggestion = suggestions[-1] #custom words appear in the end
			if suggestion == target_string:
				return True
	return False

