#!/bin/bash

parallel convert '-trim -threshold 50% {} {.}.png' ::: documents/b/*.pdf
