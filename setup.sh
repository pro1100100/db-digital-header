#!/bin/bash
sudo apt-get install -y tesseract-ocr
sudo apt-get install -y build-essential autoconf libtool pkg-config python-opengl python-imaging python-pyrex python-pyside.qtopengl idle-python2.7 qt4-dev-tools qt4-designer libqtgui4 libqtcore4 libqt4-xml libqt4-test libqt4-script libqt4-network libqt4-dbus python-qt4 python-qt4-gl libgle3 python-dev
sudo apt-get install -y libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk
#sudo easy_install greenlet
#sudo easy_install gevent
sudo apt-get install -y libblas-dev liblapack-dev libatlas-base-dev gfortran
sudo apt-get install -y python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose
sudo apt-get install -y parallel
#sudo apt-get install -y libxml2-dev libxslt-dev

sudo pip install -r requirements.txt
