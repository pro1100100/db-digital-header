#!/usr/bin/env python

import os.path

from lib.share._image_math import *
from lib.dataItems.ItemFinder import ItemFinder

img_file_path = sys.argv[1]
plan_id = sys.argv[2]
program_path = os.path.dirname(os.path.abspath(__file__))
image_path = os.path.join(program_path, img_file_path)

#img = cv2.imread(image_path,0)
#visualise_lines(img)


print(image_path)
it_finder = ItemFinder(plan_id, img_file_path)
print it_finder.find_items()